from bluesky import RunEngine
from bluesky.callbacks.zmq import Publisher

#initiate RunEngine
RE = RunEngine({})

#define ip and port for the publisher
_host = "localhost:5577"

#create publisher to broadcast documents via 0mq
publisher = Publisher(_host) 

#subscribe the publisher to the document stream
RE.subscribe(publisher)