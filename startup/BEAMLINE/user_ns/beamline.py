from bluesky_queueserver import parameter_annotation_decorator
from ophyd.sim import det1, det2, det3, det4, motor1, motor2, motor, noisy_det, SynAxis, SynGauss
from bluesky_queueserver.manager.profile_ops import devices_from_nspace, _prepare_devices
from collections.abc import Iterable

motor1._ophyd_labels_.add('mono')
det1._ophyd_labels_.add(motor1.name)
det1._ophyd_labels_.add('delay')
det2._ophyd_labels_.add(motor1.name)
det3._ophyd_labels_.add(motor2.name)
det4._ophyd_labels_.add(motor2.name)

UE48_PGM = SynAxis(name='MonoA', labels={'motors, mono'})
U17_DCM = SynAxis(name='MonoB', labels={'motors, mono'})
U17_PGM = SynAxis(name='MonoC', labels={'motors, mono'})
kthA = SynGauss('kthA', UE48_PGM, 'MonoA', center=0, Imax=5, sigma=0.5, labels={'detectors', 'TEY'})
kthB = SynGauss('kthB', U17_DCM, 'MonoB', center=0, Imax=5, sigma=0.5, labels={'detectors', 'TFY'})
kth1 = SynGauss('kth1', motor, 'motor', center=0, Imax=1, noise='uniform', sigma=1, noise_multiplier=0.1, labels={'detectors', 'ES1', 'I0'})
kth2 = SynGauss('kth2', motor, 'motor', center=0, Imax=1, noise='uniform', sigma=1, noise_multiplier=0.1, labels={'detectors', 'ES2', 'PFY'})

#OAESE 
oaese_tfy = SynGauss('oaese_tfy', motor1, 'motor1', center=0, Imax=5, sigma=0.5, labels={'detectors', 'TFY'})
oaese_mca = SynGauss('oaese_mca', motor1, 'motor1', center=0, Imax=5, sigma=0.5, labels={'detectors', 'PFY'})

#SISSY1
s1_chan = SynGauss('s1_chan', motor1, 'motor1', center=0, Imax=5, sigma=0.5, labels={'detectors', 'TFY'})
s1_tey = SynGauss('s1_tey', motor1, 'motor1', center=0, Imax=5, sigma=0.5, labels={'detectors', 'TEY'})


#from typing import List, Iterable, Sequence
import ophyd
from bluesky.plans import count, scan
import typing
from bluesky import protocols

@parameter_annotation_decorator({
    "parameters": {
        "detectors": {
            "annotation": "typing.List[str]",
            "convert_device_names": True,
        },
        "delay": {
            "annotation": "float",
            "default": 0.1,
            "min": 0.0,
            "max": 1.0,
            "step": 0.2,
        }
    }
})
def new_count(detectors, num:int, delay:float=0.1, *, md:dict):
    yield from count(detectors, num, delay)

@parameter_annotation_decorator({
    "parameters": {
        "endstation": {
            "annotation": "Endstation",
            "enums": {"Endstation": ["SISSY1", "OAESE"]},
        },
        "detectors": {
            "annotation": "typing.List[str]",
            "convert_device_names": True,
        },
        "num": {
            "annotation": "int",
            "default": 1,
            "min": 1,
            "max": 20,
            "step": 1,
        },
        "delay": {
            "annotation": "float",
            "default": 0.1,
            "min": 0.0,
            "max": 2.0,
            "step": 0.1,
        },
        "stepwise": {
            "annotation": "bool",
        },
    }
})
def dummy_plan(endstation, detectors, num:int=1, delay:float=0.1, stepwise:bool=False, md:dict={}):
    print('Wild things happening...')
    yield from count(detectors, num, delay)

def get_device_component_names(device):
    if hasattr(device, "component_names"):
        component_names = device.component_names
        if not isinstance(component_names, Iterable):
            component_names = []
    else:
        component_names = []
    return component_names

max_depth = 50
def get_device_components(device, device_name, *, depth=0):
    comps = get_device_component_names(device)
    components = {}
    if not max_depth or (depth < max_depth - 1):
        for comp_name in comps:
            try:
                if hasattr(device, comp_name):
                    c = getattr(device, comp_name)
                    desc = get_device_components(c, device_name + "." + comp_name, depth=depth + 1)
                    components[comp_name] = desc
            except Exception as ex:
                print(ex)
        if components:
            return components
    return

@parameter_annotation_decorator({
    "parameters": {
        "end_station": {
            "annotation": "Endstation",
            "enums": {"Endstation": ["SISSY1", "OAESE"]},
        },
        "mono": {
            "annotation": "mono",
            "enums": {"mono": ["UE48_PGM", "U17_DCM", "U17_PGM"]},
            "convert_device_names": True,
        },
        "mode": {
            "description": "mono",
            "annotation": "typing.Union[UE48_PGM, U17_DCM, U17_PGM]",
            "enums": {
                "UE48_PGM": ["continuous", "stepwise"],
                "U17_DCM": ["stepwise"],
                "U17_PGM": ["continuous", "stepwise"]
            }
        },
        "harmonic": {
            "annotation": "Harmonic", # The annotation name is used later to refer to it if required
            "enums": {"Harmonic": ["1", "3","5","7","Max Flux", "Auto"]},
        },
        "slit_width": {
            "description": "mono",
            "annotation": "typing.Union[UE48_PGM, U17_DCM, U17_PGM, float, NoneType]",
            "enums": {
                "UE48_PGM": ['widget:float'],
                "U17_DCM": ['widget:None'], # Somthing to say that if the DCM is selected, then this option should be disabled/ greyed out
                "U17_PGM": ['widget:float']
            }
        },
        "step_size": {
            "description": "mode",
            "annotation": "typing.Union[continuous, stepwise, float, NoneType]",
            "enums": {
                "stepwise": ['widget:float'],
                "continuous": ['widget:None'] # Somthing to say that if the DCM is selected, then this option should be disabled/ greyed out
            }
        },
        "velocity": {
            "description": "mode",
            "annotation": "typing.Union[continuous, stepwise, float, NoneType]",
            "enums": {
                "stepwise": ['widget:None'],
                "continuous": ['widget:float'] # Somthing to say that if the DCM is selected, then this option should be disabled/ greyed out
            }
        },
    }
})
def demo_plan(end_station, mono, mode,harmonic, order: int, slit_width: float, start: float, stop: float,step_size: int,velocity: float,use_shutter: bool, md: dict):
    nspace = globals()
    devs = devices_from_nspace(nspace)
    devices = {}
    for name, dev in devs.items():
        devices[name] = dev
        devices = {**devices, **get_device_components(dev, name)}

    dets = {}
    dets['TEY'] = []
    dets['TFY'] = []
    dets['I0'] = []
    dets['PFY'] = []

    for name, dev in devices.items():
        if hasattr(dev, '_ophyd_labels_'):
            if end_station in dev._ophyd_labels_:
                for key in dets.keys():
                    if key in dev._ophyd_labels_:
                        dets[key].append(dev)
            
    print('Endstation:', end_station, '|', 'Mono:', mono.name)
    for key, val in dets.items():
        print(key, ':', [v.name for v in val])

    yield from count([noisy_det], 10, 0.1)